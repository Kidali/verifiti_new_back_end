<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stickers extends Model
{
    protected $hidden = [
        'deleted_at', 'updated_at'
    ];

    protected $fillable = [
        'status'
    ];

    public function product()
    {
        return $this->belongsTo('App\Products', 'product_id', 'id');
    }
}
