<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Session;
use \App\Faq;

class WebsiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $faqs = Faq::paginate(4);
        return view('website.index', compact('faqs'));
    }

    public function deletefaq($id)
    {
        $faq = Faq::find($id);

        try {
            $faq->delete();
            if ($faq) {
                Session::flash("status", "Successfully deleted faq!");
                return Redirect::to('manage-website-content');
            }

            if (!$results) {
                Session::flash("status", "ssue with deleting faq!");
                return Redirect::to('manage-website-content');
            }
        } catch (\Throwable $th) {
            Session::flash("status", "The faq does not exists or it has been deleted!");
            return Redirect::to('manage-website-content');
        }
    }

    public function editfaq($id)
    {
        $faqs = Faq::paginate(4);
        $faq = Faq::find($id);
        return view('website.edit', compact('faq', 'faqs'));
    }

    public function updatefaq(Request $request)
    {
        $faq = Faq::find($request->id);
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->save();

        if ($faq) {
            Session::flash('status', 'Faq updated successfully!');
            return Redirect::back();
        } else {
            Session::flash('status', 'Issue updating the faq!');
            return Redirect::back();
        }
    }

    public function createfaq(Request $request)
    {
        $faq = new Faq;
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->save();

        if ($faq) {
            Session::flash('status', 'Faq added successfully!');
            return Redirect::back();
        } else {
            Session::flash('status', 'Issue adding the faq!');
            return Redirect::back();
        }
    }
}
