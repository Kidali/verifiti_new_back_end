<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Session;
use \App\Products;
use \App\User;

class ClientsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::where('user_type', 2)->get();
        return view('clients.index', compact('users'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        $products = Products::where('user_id', $id)->get();

        if ($user->user_type == 2) {
            return view('clients.edit', compact('user', 'products'));
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function archive($id)
    {
        $user = User::where('id', $id)->delete();

        try {
            if ($user) {
                Session::flash('status', 'User archived successfully!');
                return Redirect::to('manage-partners');
            }

            if (!$user) {
                Session::flash('status', 'Issue archiving the user!');
                return Redirect::to('manage-partners');
            }
        } catch (\Throwable $th) {
            Session::flash('status', 'Issue archiving the user, make sure the person exists!');
            return Redirect::to('manage-partners');
        }
    }

    public function createclient(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->phone_number = $request->phone_number;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->password = 'n/a';
        $user->user_type = 2;

        $user->save();
        if ($user) {
            Session::flash('status', 'User added successfully!');
            return Redirect::back();
        } else {
            Session::flash('status', 'Issue adding the user!');
            return Redirect::back();
        }
    }

    public function updateclient(Request $request)
    {
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->phone_number = $request->phone_number;
        $user->email = $request->email;
        $user->gender = $request->gender;

        $user->save();
        if ($user) {
            Session::flash('status', 'User updated successfully!');
            return Redirect::back();
        } else {
            Session::flash('status', 'Issue updating the user!');
            return Redirect::back();
        }
    }
}
