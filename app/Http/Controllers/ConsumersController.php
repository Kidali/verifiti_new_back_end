<?php

namespace App\Http\Controllers;
use DB;
use Session;
use Mail;
use Redirect;
use Illuminate\Http\Request;
use \App\User;
use \App\Scanned;
use \App\Points;
class ConsumersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $users = User::where('user_type', 3)->get();
        return view('consumers.index', compact('users'));
    }

    public function edit($id) {
        $user = User::find($id);
        $canneds = Scanned::where('user_id', $id)->paginate(10);
        $points = Points::where('user_id', $id)->get();

        if($user->user_type == 3) {
            return view('consumers.edit', compact('user', 'canneds', 'points'));
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function archive($id) {
        $user = User::find($id);

        try {
            $user->delete();
            if ($user) {
                Session::flash('status', 'User deleted successfully!');
                return Redirect::to('manage-users');
            }

            if (!$user) {
                Session::flash('status', 'Issue deleting the user!');
                return Redirect::to('manage-users');
            }
        } catch (\Throwable $th) {
            Session::flash('status', 'Issue deleting the product, make sure it exists!');
            return Redirect::to('manage-users');
        }
    }
}
