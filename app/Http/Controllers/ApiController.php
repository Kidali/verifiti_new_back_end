<?php

namespace App\Http\Controllers;

use App\Locations;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use \App\Chat;
use \App\Chatthread;
use \App\Comments;
use \App\Faq;
use \App\Points;
use \App\Stats;
use \App\Website;
use \App\Auth;
class ApiController extends Controller
{
    public function locations()
    {
        $locations = Locations::all();
        return response()->json([
            'status' => 'success',
            'message' => 'location data successfully pulled',
            'data' => $locations,
        ], 200);
    }

    public function drivers($location, $user_type = 2)
    {
        $drivers = User::where('user_type', $user_type)->where('location', $location)->get();
        return response()->json([
            'status' => 'success',
            'message' => 'Drivers data successfully pulled',
            'count' => count($drivers),
            'data' => $drivers,
        ], 200);
    }

    public function addproducts(Request $request)
    {
        $data = array(
            "name" => $request->name,
            "code" => $request->code,
            "price" => $request->price,
            "size" => $request->size,
            "color" => $request->color,
            "origin" => $request->origin,
            "manufactured_date" => $request->manufactured_date,
            "expiration_date" => $request->expiration_date,
            "user_id" => $request->user_id,
            "created_at" => Carbon::now(),
        );

        $results = DB::table('products')->insert($data);

        if ($results) {
            return response()->json([
                'status' => 'success',
                'message' => 'Product added successfully!!',
            ], 200);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Product adding failed!!',
            ], 200);
        }
    }

    public function archiveproduct($id)
    {
        $product = Products::find($id);

        try {
            $product->delete();
            if ($product) {
                return $this->feedback(201, 'Successfully archived product!');
            }

            if (!$product) {
                return $this->feedback(202, 'Issue with archiving product!');
            }
        } catch (\Throwable $th) {
            return $this->feedback(202, 'The product does not exists or it has been archived!');
        }
    }

    public function faqs()
    {
        $faqs = Faq::all();
        return response()->json([
            'status' => 'success',
            'message' => 'faqs data successfully pulled',
            'data' => $faqs,
        ], 200);
    }

    public function sendsupport(Request $request)
    {
        $checkthread = Chatthread::where('thread', 'thread-' . $request->sender_id)->count();

        if ($checkthread == 0) {
            $threadchat = new Chatthread;
            $threadchat->user_id = $request->sender_id;
            $threadchat->admin_id = 1;
            $threadchat->thread = 'thread-' . $request->sender_id;
            $threadchat->save();

            if ($threadchat) {
                $chat = new Chat;
                $chat->thread = $threadchat->id;
                $chat->sender_id = $request->sender_id;
                $chat->receiver_id = 1;
                $chat->message = $request->message;
                $chat->save();

                if ($chat) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Message sent successfully',
                        'data' => 1,
                    ], 200);
                } else {
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'Message sending failed failed!',
                        'data' => 0,
                    ], 200);
                }
            } else {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'Issue initiating thread!',
                    'data' => 0,
                ], 200);
            }
        } else {
            $thread = Chatthread::where('thread', 'thread-' . $request->sender_id)->first();

            $chat = new Chat;
            $chat->thread = $thread->id;
            $chat->sender_id = $request->sender_id;
            $chat->receiver_id = 1;
            $chat->message = $request->message;
            $chat->save();

            if ($chat) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Message send successfully',
                    'data' => 1,
                ], 200);
            } else {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'Message sending failed failed!',
                    'data' => 0,
                ], 200);
            }
        }
    }

    public function getAllMessages($id) {
        $thread = Chatthread::where('thread', 'thread-' . $id)->first();

        $data = DB::table('chats')->where('thread', $thread->id)->orderBy('created_at', 'desc')->get();
        return response()->json([
            'status' => 'success',
            'message' => 'All messages!',
            'data' => $data,
        ], 200);
    }

    public function about()
    {
        $website = Website::all();
        return response()->json([
            'status' => 'success',
            'message' => 'website data successfully pulled',
            'data' => $website,
        ], 200);
    }

    public function createcomment(Request $request)
    {
        $resp = new Comments;
        $resp->user_id = $request->user_id;
        $resp->product_id = $request->product_id;
        $resp->comment = $request->comment;
        $resp->save();

        if ($resp) {
            return response()->json([
                'status' => 'success',
                'message' => 'Your comment received successfully',
                'data' => 1,
            ], 200);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Comment not received!',
                'data' => 0,
            ], 200);
        }
    }

    public function createstats(Request $request)
    {
        $resp = new Stats;
        $resp->user_id = $request->user_id;
        $resp->product_id = $request->product_id;
        $resp->medium = $request->medium;
        $resp->save();

        if ($resp) {
            return response()->json([
                'status' => 'success',
                'message' => 'Sharing stats received successfully',
                'data' => 1,
            ], 200);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Sharing stats not received!',
                'data' => 0,
            ], 200);
        }
    }

    public function redeempoints(Request $request)
    {
        $resp = DB::table('users')->decrement('points', $request->point);

        $points = new Points;
        $points->user_id = $request->user_id;
        $points->points = $request->point;
        $points->status = 1;
        $points->save();

        if ($resp) {
            return response()->json([
                'status' => 'success',
                'message' => 'Points redeem successfully',
                'data' => 1,
            ], 200);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Redeem failed!',
                'data' => 0,
            ], 200);
        }
    }

    public function addpoints(Request $request)
    {
        $resp = DB::table('users')->increment('points', $request->point);

        $points = new Points;
        $points->user_id = $request->user_id;
        $points->points = $request->point;
        $points->status = 0;
        $points->save();

        if ($resp) {
            return response()->json([
                'status' => 'success',
                'message' => 'Points earned successfully',
                'data' => 1,
            ], 200);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Points earning failed failed!',
                'data' => 0,
            ], 200);
        }
    }

    public function registershares(Request $request)
    {
        DB::table('socialsharings')->where('product_id', $request->product_id)->increment($request->media, 1);
    }
}
