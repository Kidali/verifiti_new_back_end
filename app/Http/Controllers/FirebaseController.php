<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function firebaseConfig() {
        // $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/productauthentication-firebase-adminsdk-2gikf-8428a66420.json');
        // return $firebase = (new Factory)
        //     ->withServiceAccount($serviceAccount)
        //     ->withDatabaseUri('https://productauthentication.firebaseio.com/')
        //     ->create();

        //     $database = $this->firebaseConfig()->getDatabase();

        // $newPost = $database
        //     ->getReference('chat/client-10');
        //     // ->push([
        //     //     'from' => 'admin-01',
        //     //     'message' => 'Your message goes here, hope you are doing good!',
        //     //     'status' => 0,
        //     //     'timestamp' => date("Y-m-d H:i:s")
        //     // ]);
        // echo '<pre>';
        // $data = json_encode($newPost->getvalue());
        // foreach($data as $d) {
        //     echo $d;
        // }
    }
}
