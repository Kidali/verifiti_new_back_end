<?php

namespace App\Http\Controllers;
use \App\User;
use \App\Products;
use \App\Stickers;
use \App\DB;
use \App\Faq;
use \App\website;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $productCount = Products::count();
        $clientCount = User::where('user_type', 2)->count();
        $customerCount = User::where('user_type', 3)->count();
        $stickersCount = Stickers::count();
        return view('home', compact('productCount', 'stickersCount', 'clientCount', 'customerCount'));
    }
}
