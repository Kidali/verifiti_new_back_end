<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Illuminate\Http\Request;
use Redirect;
use Session;
use \App\User;

class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        return view('settings.index');
    }

    public function updatecredentials(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $oldpassword = $request->oldpassword;

        if ($password !== '') {

            if (Hash::check($oldpassword, Auth::user()->password)) {
                $user = User::find(Auth::user()->id);
                $user->email = $email;
                $user->password = Hash::make($password);
                $user->save();
            }

        } else {
            $user = User::find(Auth::user()->id);
            $user->email = $email;
            $user->save();
        }

        if ($user) {
            Session::flash('status', 'Settings updated successfully!');
            return Redirect::back();
        }

        if (!$user) {
            Session::flash('status', 'Settings updating had an issue!');
            return Redirect::back();
        }
    }

}
