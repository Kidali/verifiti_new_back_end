<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;
use \App\Products;
use \App\Stickers;
use \App\Comments;
use \App\User;
use App\Charts\SampleChart;
use \App\Socialsharing;
class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Products::all();
        $partners = User::where('user_type', 2)->get();
        return view('products.index', compact('products', 'partners'));
    }

    public function manageproduct($id)
    {
        $chart = new SampleChart;

        $product = Products::find($id);
        $product->scanned;
        $product->user;

        $socialsharings = Socialsharing::where('product_id', $id)->first();

        $today_users = $product->scanned->where('created_at', today())->where('status', 1)->count();
        $yesterday_users = $product->scanned->where('created_at', today()->subDays(1))->where('status', 1)->count();
        $users_2_days_ago = $product->scanned->where('created_at', today()->subDays(2))->where('status', 1)->count();

        $today_users2 = $product->scanned->where('created_at', today())->where('status', 0)->count();
        $yesterday_users2 = $product->scanned->where('created_at', today()->subDays(1))->where('status', 0)->count();
        $users_2_days_ago2 = $product->scanned->where('created_at', today()->subDays(2))->where('status', 0)->count();

        $chart->labels(['2 days ago', 'Yesterday', 'Today']);
        $chart->dataset('Genuine', 'line', [$users_2_days_ago, $yesterday_users, $today_users])->options([
            'backgroundColor' => '#b7a79f',
        ]);
        $chart->dataset('Repeat', 'line', [$users_2_days_ago2, $yesterday_users2, $today_users2])->options([
            'backgroundColor' => '#0098e1',
        ]);

        $stickers = Stickers::where('product_id', $id)->get();
        $comments = Comments::where('product_id', $id)->get();
        $partners = User::where('user_type', 2)->get();

        return view('products.manage', compact('product', 'stickers', 'comments', 'partners', 'chart', 'socialsharings'));
    }

    public function createproduct(Request $request)
    {

        $data = [
            'name' => $request->productName,
            'code' => $request->productCode,
            'user_id' => $request->manufacturer,
            'price' => $request->price,
            'size' => $request->size,
            'origin' => $request->origin,
            'manufactured_date' => date("Y-m-d", strtotime($request->manufactured_date)),
            'expiration_date' => date("Y-m-d", strtotime($request->expiration_date)),
        ];

        $validator = Validator::make(
            [
                'productName' => $request->productName,
                'productCode' => $request->productCode,
                'manufacturer' => $request->manufacturer,
                'price' => $request->price,
                'size' => $request->size,
                'origin' => $request->origin,
                'manufactured_date' => $request->manufactured_date,
                'expiration_date' => $request->expiration_date,
            ],
            [
                'productName' => 'required|min:3',
                'productCode' => 'required|min:3',
                'manufacturer' => 'required|integer',
                'price' => 'required|integer',
                'size' => 'required',
                'origin' => 'required',
                'manufactured_date' => 'required',
                'expiration_date' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'status' => 'errors',
                'errors' => $validator->errors(),
            ]);
        }

        $product = new Products($data);
        $resp = $product->save();

        if ($resp) {

            $social = new Socialsharing();
            $social->product_id = $product->id;

            $social->save();

            Session::flash('status', 'Product added successfully!');
            return Redirect::back();
        } else {
            Session::flash('status', 'Issue adding the product!');
            return Redirect::back();
        }
    }

    public function archive($id)
    {
        $properties = Products::where('id', $id)->delete();

        try {
            if ($properties) {
                Session::flash('status', 'Product archived successfully!');
                return Redirect::to('manage-products');
            }

            if (!$properties) {
                Session::flash('status', 'Product archiving unsuccessful!');
                return Redirect::to('manage-products');
            }
        } catch (\Throwable $th) {
            Session::flash('status', 'The product does not exists or it has been archived already!');
            return Redirect::to('manage-products');
        }
    }

    public function updateproduct(Request $request)
    {
        $product = Products::find($request->id);
        $product->name = $request->productName;
        $product->code = $request->productCode;
        $product->user_id = $request->manufacturer;
        $product->price = $request->price;
        $product->size = $request->size;
        $product->origin = $request->origin;
        $product->manufactured_date = date("Y-m-d", strtotime($request->manufactured_date));
        $product->expiration_date = date("Y-m-d", strtotime($request->expiration_date));
        $product->save();

        if ($product) {
            Session::flash('status', 'Product updated successfully!');
            return Redirect::back();
        } else {
            Session::flash('status', 'Issue updating the product!');
            return Redirect::back();
        }
    }
}
