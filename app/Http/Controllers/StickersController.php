<?php

namespace App\Http\Controllers;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use \App\Products;
use \App\Scanned;
use \App\Stickers;
use \App\User;
use Session;
use Redirect;
class StickersController extends Controller
{
    public function index() {
        $stickers = Stickers::all();
        $products = Products::all();
        $clients = User::all();

        $stickersused = Stickers::where('status', 0)->count();
        $stickersnotused = Stickers::where('status', 1)->count();

        return view('stickers.stickers', compact('stickers', 'products', 'clients', 'stickersused', 'stickersnotused'));
    }

    public function stickers()
    {
        $stickers = Stickers::all();
        return response()->json([
            'status' => 'success',
            'message' => 'All stickers pulled!',
            'data' => $stickers,
        ], 200);
    }

    public function scannedstickers($id)
    {
        $scanned = Scanned::where("user_id", $id)->get();

        $data = array();

        foreach ($scanned as $key) {
            $d = array();
            $product = Products::find($key->product_id);

            $d["name"] = $product->name;
            $d["code"] = $product->code;
            $d["price"] = $product->price;
            $d["color"] = $product->color;
            $d["size"] = $product->size;
            $d["manufactured_date"] = $product->manufactured_date;
            $d["expiration_date"] = $product->expiration_date;
            $d["scanned_date"] = $key->created_at;
            $d["status"] = $key->status;

            array_push($data, $d);
        }

        json_encode($data, true);

        return response()->json([
            'status' => 'success',
            'message' => 'All scanned stickers!',
            'data' => $data,
        ], 200);
    }

    public function archivescanned($id)
    {
        $properties = DB::table('scanned')->where('user_id', $id)->delete();

        try {
            if ($properties) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Successfully delete scanned history!',
                    'data' => 1,
                ], 201);
            }

            if (!$properties) {

                return response()->json([
                    'status' => 'fail',
                    'message' => 'Issue with deleting scanned!',
                    'data' => 0,
                ], 202);
            }
        } catch (\Throwable $th) {

            return response()->json([
                'status' => 'fail',
                'message' => 'The scanned does not exists or it has been deleted!',
                'data' => 0,
            ], 200);
        }
    }

    public function archivesticker($id)
    {
        $properties = Stickers::find($id);

        try {
            $properties->delete();
            if ($properties) {
                return $this->feedback(201, 'Successfully archived sticker!');
            }

            if (!$properties) {
                return $this->feedback(202, 'Issue with archiving sticker!');
            }
        } catch (\Throwable $th) {
            return $this->feedback(202, 'The sticker does not exists or it has been archived!');
        }
    }

    public function updatesticker()
    {

    }

    public function searchBarcode($barcode, $user_id)
    {
        $results = Stickers::where('barcode', $barcode)->first();
        $user = User::find($user_id);

        if($user->gender == '') {
            $gender = 'n/a';
        } else {
            $gender = $user->gender;
        }

        $county = DB::table('locations')->where('id', $user->location)->first();
        $location = $county->name;



        if (count((array) $results)) {
            $results->product;

            // check if barcode is used
            $slect = Stickers::select(['status', 'id', 'product_id'])->where('barcode', $barcode)->first();

            // update status upon scanning
            if ($slect->status == 1) {
                $results->update(["status" => 0]);
                Scanned::insert([
                    'user_id' => $user_id,
                    'sticker_id' => $slect->id,
                    'status' => 1,
                    'product_id' => $slect->product_id,
                    'gender' => $gender,
                    'county' => $location->name,
                    'created_at' => Carbon::now(),
                ]);
                $user = DB::table('users')->where('id', $user_id)->increment('points', 5);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Well done! The code is genuine.',
                    'data' => $results,
                ], 200);
            } else {
                Scanned::insert([
                    'user_id' => $user_id,
                    'sticker_id' => $slect->id,
                    'status' => 0,
                    'gender' =>  $gender,
                    'county' => $location,
                    'product_id' => $slect->product_id,
                    'created_at' => Carbon::now(),
                ]);
                return response()->json([
                    'status' => 'Fail',
                    'message' => 'Repeat-Error! The code has already been verified ',
                    'data' => $results,
                ], 200);
            }
        } else {
            Scanned::insert([
                'user_id' => $user_id,
                'sticker_id' => 0,
                'status' => 1,
                'product_id' => 0,
                'gender' => $gender,
                'county' => $location,
                'invalid_code' => $barcode,
                'created_at' => Carbon::now(),
            ]);
            return response()->json([
                'status' => 'failed',
                'message' => 'Invalid-Error! The code does not exist in our database/The code does not exist.',
                'data' => 0,
            ], 200);
        }
    }

    public function generatesticker(Request $request)
    {
        $data = array();
        $number = $request->number;
        $user_id = $request->user_id;
        $product_id = $request->product_id;
        $serial_number = str_random(10);
        $digits = 16;

        for ($i = 1; $i < $number + 1; $i++) {
            //  Generate barcode
            $barcode = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

            //  Generate database info
            $info = array(
                "barcode" => $barcode,
                "product_id" => $product_id,
                "serial_number" => $serial_number,
                "created_at" => Carbon::now(),
            );

            array_push($data, $info);
        }

        Stickers::insert($data);

        Session::flash('status', 'Sticker(s) generated successfully');
        return Redirect::back();
    }

}
