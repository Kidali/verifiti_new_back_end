<?php

namespace App\Http\Controllers;

use App\Charts\SampleChart;
use App\Exports\ProductsDataExport;
use App\Exports\UsersExport;
use Excel;
use Illuminate\Http\Request;
use PDF;
use Redirect;
use \App\Products;
use \App\Comments;
use \App\Scanned;

class ReportController extends Controller
{
    public function exportUser()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function import()
    {
        return Excel::import(new UsersImport, 'users.xlsx');
    }

    public function exportProductData(Request $request)
    {
        $startTime = $request->startTime;
        $endTime = $request->endTime;

        $product_id = $request->product_id;
        $datatype = $request->datatype;
        $startDate = $request->selected_date . ' ' . $startTime;
        $endDate = $request->selected_date2 . ' ' . $endTime;
        $exportformat = $request->exportformat;

        $date1 = date('Y-m-d H:i:s', strtotime($startDate));
        $date2 = date('Y-m-d H:i:s', strtotime($endDate));

        if ($exportformat == 'xls') {
        return (new ProductsDataExport($product_id, $datatype, $date1, $date2))->download('data.xlsx');
        // return Excel::download(new ProductsDataExport, 'stickers.xlsx');
        } else {
            // $comments = Comments::where('product_id', $product_id)->where('created_at', '>', $date1)->where('created_at', '<', $date2)->get();
            // $product = Products::find($product_id);

            return (new ProductsDataExport($product_id, $datatype, $date1, $date2))->download('stickers.xlsx');

        $pdf = PDF::loadView('pdf.comments', compact('comments', 'product'));
        return $pdf->stream('comments.pdf');
        }
    }

    public function downloadscannedreport($date, $date2, $id, $size, $gender, $county)
    {
        $data = Scanned::whereDate('created_at', '>', $date)->whereDate('created_at', '<', $date2)->where('status', 1)->where('product_id', $id)->where('size', $size)->where('gender', $gender)->where('county', $county)->get();

        return (new ProductsDataExport($id, 1, $date, $date2))->download('data.xlsx');
        // $pdf = PDF::loadView('pdf.scanned', compact('data', 'id', 'gender', 'county', 'date', 'date2', 'size'));
        // return $pdf->stream('scanned.pdf');
    }

    public function index()
    {
        $chart = new SampleChart;
        $scans = Scanned::where('invalid_code', '>', 0)->get();
        $products = Products::all();

        $today_users = Scanned::whereDate('created_at', today())->count();
        $yesterday_users = Scanned::whereDate('created_at', today()->subDays(1))->count();
        $users_2_days_ago = Scanned::whereDate('created_at', today()->subDays(2))->count();

        $today_users = Scanned::whereDate('created_at', today())->where('status', 1)->count();
        $yesterday_users = Scanned::whereDate('created_at', today()->subDays(1))->where('status', 1)->count();
        $users_2_days_ago = Scanned::whereDate('created_at', today()->subDays(2))->where('status', 1)->count();

        $today_users2 = Scanned::whereDate('created_at', today())->where('status', 0)->count();
        $yesterday_users2 = Scanned::whereDate('created_at', today()->subDays(1))->where('status', 0)->count();
        $users_2_days_ago2 = Scanned::whereDate('created_at', today()->subDays(2))->where('status', 0)->count();

        $chart->labels(['2 days ago', 'Yesterday', 'Today']);
        $chart->dataset('Genuine', 'line', [$users_2_days_ago, $yesterday_users, $today_users])->options([
            'backgroundColor' => '#b7a79f',
        ]);
        $chart->dataset('Repeat', 'line', [$users_2_days_ago2, $yesterday_users2, $today_users2])->options([
            'backgroundColor' => '#0098e1',
        ]);
        return view('reports.index', compact('chart', 'scans', 'products'));
    }

    public function filterreport(Request $request)
    {
        $selected_date = date('Y-m-d', strtotime($request->selected_date));
        $selected_date2 = date('Y-m-d', strtotime($request->selected_date2));

        if ($request->size == '') {
            $size = 0;
        } else {
            $size = $request->size;
        }

        $gender = $request->gender;
        $county = $request->county;

        return Redirect::to('filter-scanned/' . $selected_date . '/' . $selected_date2 . '/' . $request->product_id . '/' . $size . '/' . $gender . '/' . $county . '/scanned');
    }

    public function filter($date, $date2, $id, $size, $gender, $county)
    {
        $chart = new SampleChart;

        $genuine = Scanned::whereDate('created_at', '>', $date)->whereDate('created_at', '<', $date2)->where('status', 1)->where('product_id', $id)->where('size', $size)->where('gender', $gender)->where('county', $county)->count();
        $repeat = Scanned::whereDate('created_at', '>', $date)->whereDate('created_at', '<', $date2)->where('status', 0)->where('product_id', $id)->where('size', $size)->where('gender', $gender)->where('county', $county)->count();

        $chart->labels([$date, $date2]);
        $chart->dataset('Genuine', 'line', [$genuine])->options([
            'backgroundColor' => '#b7a79f',
        ]);
        $chart->dataset('Repeat', 'line', [$repeat])->options([
            'backgroundColor' => '#0098e1',
        ]);
        $products = Products::all();
        $product = Products::find($id);
        return view('reports.filter', compact('chart', 'date', 'date2', 'product', 'products', 'gender', 'county', 'id', 'size'));
    }

}

// php artisan makxe:export UsersExport --model=App\\User
// php artisan make:import UsersImport --model=App\\User
// https://charts.erik.cat/customize_datasets.html#generic-customization
