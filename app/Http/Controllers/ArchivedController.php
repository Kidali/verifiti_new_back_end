<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use DB;
use \App\Products;
use \App\User;
class ArchivedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Products::onlyTrashed()->get();
        $users = User::onlyTrashed()->get();
        return view('archived.index', compact('products', 'users'));
    }

    public function productrestore($id) {
        $products = Products::withTrashed()->where('id', $id)->restore();
        if ($products) {
            Session::flash('status', 'Product restored successfully!');
            return Redirect::back();
        } else {
            Session::flash('status', 'Issue restoring the product!');
            return Redirect::back();
        }
    }

    public function restoreuser($id) {
        $products = User::withTrashed()->where('id', $id)->restore();
        if ($products) {
            Session::flash('status', 'user restored successfully!');
            return Redirect::back();
        } else {
            Session::flash('status', 'Issue restoring the user!');
            return Redirect::back();
        }
    }
}
