<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Chatthread;
use Auth;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;
use \App\User;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')->where('user_type', 3)->get();
        $chats = Chat::orderBy('created_at', 'desc')->get();
        $threads = Chatthread::all();
        return view('chat.index', compact('chats', 'users', 'threads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checkthread = Chatthread::where('thread', 'thread-' . $request->receiver_id)->count();

        if ($checkthread == 0) {
            $chat = new Chatthread;
            $chat->user_id = $request->receiver_id;
            $chat->admin_id = Auth::user()->id;
            $chat->thread = 'thread-' . $request->receiver_id;
            $chat->save();

            if ($chat) {
                Session::flash('status', 'New thread created successfully');
                return Redirect::back();
            } else {
                Session::flash('status', 'Thread creation failed!');
                return Redirect::back();
            }
        } else {
            Session::flash('status', 'Thread already exists@!');
            return Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $thread = Chatthread::where('id', $id)->first();
        $user = User::where('id', $thread->user_id)->first();
        $chats = Chat::where('thread', $id)->orderBy('created_at', 'desc')->get();

        Chat::query()->where('receiver_id', $thread->admin_id)->where('thread', $thread->id)->update(['receiver_status' => 1]);
        return view('chat.manage', compact('chats', 'thread', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sendmessage(Request $request)
    {
        $chat = new Chat;
        $chat->thread = $request->thread;
        $chat->sender_id = $request->sender_id;
        $chat->receiver_id = $request->receiver_id;
        $chat->message = $request->message;
        $chat->save();

        if ($chat) {
            Session::flash('status', 'Message sent successfully');
            return Redirect::back();
        } else {
            Session::flash('status', 'Message sending failed!');
            return Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
