<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    protected $table = "website";

    protected $hidden = [
        'created_at', 'deleted_at', 'updated_at'
    ];
}
