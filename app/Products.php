<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use SoftDeletes;

    protected $hidden = [
        'deleted_at', 'updated_at', 'product_id'
    ];

    protected $fillable = [
        'name',
        'code',
        'user_id',
        'price',
        'size',
        'origin',
        'manufactured_date' ,
        'expiration_date'
    ];

    public function stickers()
    {
        return $this->hasMany('App\Stickers');
    }

    public function scanned()
    {
        return $this->hasMany('App\Scanned', 'product_id', 'id');
    }

    public function user()
    {
        return $this->hasMany('App\User', 'id', 'user_id');
    }
}
