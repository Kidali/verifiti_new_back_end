<?php

namespace App\Exports;

use App\Scanned;
use App\Stickers;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use App\Products;
class ProductsDataExport implements FromQuery
{
    use Exportable;

    public function __construct(int $product_id, string $datatype, string $startDate, string $endDate)
    {
        $this->product_id = $product_id;
        $this->datatype = $datatype;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function query()
    {
        if ($this->datatype == 1) {
            return Stickers::query()->where('product_id', $this->product_id)->whereBetween('created_at', [$this->startDate, $this->endDate]);
        } else if($this->datatype == 2) {
            return Scanned::query()->where('product_id', $this->product_id)->whereBetween('created_at', [$this->startDate, $this->endDate]);
        } else {
            return Products::query()->where('id', $this->product_id)->whereBetween('created_at', [$this->startDate, $this->endDate]);
        }
    }
}
// if ($this->datatype == 1) {
//     return Stickers::query()->where('product_id', $this->product_id)->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->get();
// } else {
//     return Scanned::query()->where('product_id', $this->product_id)->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->get();
// }
