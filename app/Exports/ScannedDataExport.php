<?php

namespace App\Exports;

use App\Scanned;
use App\Stickers;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class ScannedDataExport implements FromQuery
{
    use Exportable;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function query()
    {
            return $this->data;
    }
}
// if ($this->datatype == 1) {
//     return Stickers::query()->where('product_id', $this->product_id)->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->get();
// } else {
//     return Scanned::query()->where('product_id', $this->product_id)->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->get();
// }
