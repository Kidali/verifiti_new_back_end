<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scanned extends Model
{
    protected $table = "scanned";

    protected $hidden = [
        'updated_at', 'deleted_at'
    ];
}
