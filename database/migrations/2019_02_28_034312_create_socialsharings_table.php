<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialsharingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socialsharings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('facebook')->default(0);
            $table->string('whatsapp')->default(0);
            $table->string('email')->default(0);
            $table->string('others')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socialsharings');
    }
}
