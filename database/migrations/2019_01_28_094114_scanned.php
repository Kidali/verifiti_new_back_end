<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Scanned extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scanned', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('barcode_id');
            $table->boolean('status')->default(false);
            $table->string('invalid_code');
            $table->string('counter')->default(0);
            $table->string('gender', 55);
            $table->string('county', 55);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scanned');
    }
}
