@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
     {{-- hero section --}}
      <div class="col-lg-12">
         <div class="jumbotron jumbotron-fluid">
            <div class="container">
               <div class="row">
                  <article class="col-lg-6"></article>
                  <article class="col-lg-6">
                     <h4 class="titles">Manage user</h4>
                     <p class="lead">{{$user->name}}</p>
                  </article>
               </div>
            </div>
         </div>
      </div>
      {{-- @end of hero section --}}
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">

                            <div class="form-group">
                                <label class="labels">Full Names</label>
                                <input type="text" value="{{ $user->name }}" readonly class="border-none form-control">
                            </div>

                            <div class="form-group">
                                <label class="labels">Email Address</label>
                                <input type="text" value="{{ $user->email }}" readonly class="border-none form-control">
                            </div>

                            <div class="form-group">
                                <label class="labels">Phone Number</label>
                                <input type="text" value="{{ $user->phone_number }}" readonly class="border-none form-control">
                            </div>

                            <div class="form-group">
                                <label class="labels">Gender</label>
                                <input type="text" value="{{ $user->gender }}" readonly class="border-none form-control">
                            </div>

                            <div class="form-group">
                                <label class="labels">Status</label>
                                <input type="text" value="@if($user->status == 0)Not Active
                                @else Active
                                @endif" readonly class="border-none form-control">
                            </div>

                            <div class="form-group">
                                <a href="{{ url('archive-consumer') }}/{{ $user->id }}" class="btn btn-danger">
                                    DEACTIVATE USER
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group" style="margin-top: 5%;">
                <h4 align="center" class="sub-title">Points</h4>
                <div style="margin: 0 auto;" class="divider"></div>
                <div class="circle-points">
                    <h4 align="center" class="stat">{{$user->points}}</h4>
                </div>
            </div>
        </div>
        {{--  Display scanned history  --}}
        <div class="col-md-8">
            <div class="form-group">
                <h4 class="sub-title">Scanned history</h4>
                <div class="divider"></div>
            </div>
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Status</th>
                        <th>Code</th>
                        <th>Creation Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($canneds as $scanned)
                    <tr>
                        <td>{{$scanned-> id}}</td>
                        <td>
                            <?php
                               $names = DB::table('users')->select('name')->where('id', $scanned->user_id)->get();
                               foreach($names as $name) {
                                   echo $name->name;
                               }
                            ?>
                        </td>
                        <td>
                            @if($scanned->status == 0) Failed @else Success @endif
                        </td>
                        <td>
                            <?php
                               $results = DB::table('stickers')->select('barcode')->where('id', $scanned->user_id)->get();
                               foreach($results as $result) {
                                   echo $result->barcode;
                               }
                            ?>
                        </td>
                        <td>{{$scanned-> created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Status</th>
                        <th>Code</th>
                        <th>Creation Date</th>
                    </tr>
                </tfoot>
            </table>
            <br>
            {!! $canneds->links() !!}
            <br>
            <div class="form-group">
                <h4 class="sub-title">Points redeemed logs</h4>
                <div class="divider"></div>
            </div>
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Points</th>
                        <th>Status</th>
                        <th>Creation Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($points as $point)
                    <tr>
                        <td>{{$point->id}}</td>
                        <td>{{$point->points}}</td>
                        <td>
                            @if($point->status == 0) Earned @else Redeemed @endif
                        </td>
                        <td>{{$point-> created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Points</th>
                        <th>Status</th>
                        <th>Creation Date</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
