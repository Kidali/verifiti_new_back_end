@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
    {{--  hero section  --}}
        <div class="col-lg-12">
    <div class="jumbotron jumbotron-fluid">
     <div class="container">
    <div class="row">
    <article class="col-lg-6"></article>
    <article class="col-lg-6">
      <h4 class="titles">Manage users</h4>
      <p class="lead">users</p>
    </article>
   </div>
  </div>
</div>
    </div>

    {{--  @end of hero section  --}}

        <div class="col-md-12">
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Gender</th>
                        <th>Status</th>
                        <th>Creation Date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                   <td>{{ $user -> id }}</td>
                   <td>{{ $user -> name }}</td>
                   <td>{{ $user -> email }}</td>
                   <td>{{ $user -> phone_number }}</td>
                   <td>{{ $user -> gender }}</td>
                   <td>
                       @if($user->status == 0)
                          Not Active
                       @else
                          Active
                       @endif
                   </td>
                   <td>{{ $user -> created_at }}</td>
                   <td><a href="{{ url('edit') }}/{{ $user->id}}/user">Manage user</a></td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Gender</th>
                        <th>Staatus</th>
                        <th>Creation Date</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
