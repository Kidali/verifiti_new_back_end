@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        {{-- hero section --}}
        <div class="col-lg-12">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <div class="row">
                        <article class="col-lg-6"></article>
                        <article class="col-lg-6">
                            <h4 class="titles">Chat Thread</h4>
                            <p class="lead">User - @ {{$user->name}}</p>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        {{-- @end of hero section --}}
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
            <br> {{-- content --}}
            <div class="row">
                <div class="col-lg-10 offset-1">
                    <div class="row">
                        <div class="col-lg-4 left-messaging">
                            <form action="{{ url('sendmessage') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="hidden" value="{{$thread->id}}" name="thread">
                                    <input type="hidden" value="{{$thread->admin_id}}" name="sender_id">
                                    <input type="hidden" value="{{$user->id}}" name="receiver_id">
                                </div>
                                <div class="form-group">
                                    <label class="labels">Message</label>
                                    <textarea rows="9" name="message" placeholder="Your message here ..." class="border-none form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">SEND MESSAGE</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-8">
                        @foreach($chats as $chat)
                            <div class="chat-container">
                            @if($chat->sender_id == 2)
                                <div class="alert alert-primary col-lg-6 offset-lg-3" role="alert">
                                    {{$chat->message}}
                                    <br>
                                    <time class="time">{{$chat->created_at}}</time>
                                </div>
                            @else
                                <div class="alert alert-secondary col-lg-6" role="alert">
                                    {{$chat->message}}
                                    <br>
                                    <time class="time">{{$chat->created_at}}</time>
                                </div>
                            @endif
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
