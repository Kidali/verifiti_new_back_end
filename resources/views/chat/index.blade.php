@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        {{-- hero section --}}
        <div class="col-lg-12">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <div class="row">
                        <article class="col-lg-6"></article>
                        <article class="col-lg-6">
                            <h4 class="titles">Messaging area</h4>
                            <p class="lead">Chat</p>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        {{-- @end of hero section --}}
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
            <br>

            <button data-toggle="modal" data-target="#exampleModal" class="btn btn-primary">NEW THREAD</button>
            <br>
            <br> {{-- content --}}
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="chat-headers">Threads</h4>
                    <table class="table table-striped table-condensed table-bordered">
                        @foreach($threads as $thread)
                        <tr>
                            <td>{{$thread->thread}}&nbsp;&nbsp;
                                <time style="color: #4675e3;" class="time">{{$thread->created_at}}</time>
                            </td>
                            <td>
                                <?php
                            $user = DB::table('users')->where('id', $thread->user_id)->first();
                            echo '<a href="chat-thread/'.$thread->id.'">@'.$user->name.'</a>';
                            ?>
                            </td>
                            <td width="20%">unread <a>
                            <?php
                            $unreadcount = DB::table('chats')->where('receiver_id', $thread->admin_id)->where('thread', $thread->id)->where('receiver_status', 0)->count();
                            echo $unreadcount;
                            ?>
                            </a></td>
                        </tr>
                        @endforeach
                    </table>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Thread</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ url('createthread') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="labels">Select user</label>
                                <select name="receiver_id" placeholder="receiver" class="border-none form-control">
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{--
                            <div class="form-group">
                                <label class="labels">Message</label>
                                <textarea name="message" placeholder="Your message here ..." class="border-none form-control"></textarea>
                            </div> --}}
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">CREATE THREAD</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
