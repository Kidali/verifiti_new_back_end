@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        {{-- hero section --}}
        <div class="col-lg-12">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <div class="row">
                        <article class="col-lg-6"></article>
                        <article class="col-lg-6">
                            <h4 class="titles">Manage archived</h4>
                            <p class="lead">Archived</p>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        {{-- @end of hero section --}}
        <div class="col-md-10 offset-md-1">
            <div class="form-group">
                <h4 class="sub-title">Archived Products</h4>
                <div class="divider"></div>
            </div>
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Product</th>
                        <th>Code</th>
                        <th>Size</th>
                        <th>Manufacturer</th>
                        <th>Creation Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td><a>{{$product->name}}</a></td>
                        <td>{{$product->code}}</td>
                        <td>{{$product->size}}</td>
                        <td>
                            <?php
                               $results = DB::table('users')->select('name')->where('id', $product->user_id)->get();
                               foreach($results as $result) {
                                   echo $result->name;
                               }
                            ?>
                        </td>
                        <td>{{$product->created_at}}</td>
                        <td>
                            <a class="btn btn-success btn-sm" href="{{ url('product-restore') }}/{{$product->id}}">Restore</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Product</th>
                        <th>Code</th>
                        <th>Serial Number</th>
                        <th>Status</th>
                        <th>Creation Date</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            <div class="form-group">
                <h4 class="sub-title">Deactivated Users</h4>
                <div class="divider"></div>
            </div>
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>User Type</th>
                        <th>Registration Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td><a>{{$user->name}}</a></td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone_number}}</td>
                        <td>
                          @if($user->user_type == 3)
                            Users
                          @else
                            Partners
                          @endif
                        </td>
                        <td>{{$user->created_at}}</td>
                        <td>
                            <a class="btn btn-success btn-sm" href="{{ url('user-restore') }}/{{$user->id}}">Restore</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>User type</th>
                        <th>Registration Date</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
