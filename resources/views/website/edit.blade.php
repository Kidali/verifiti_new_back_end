@extends('layouts.app') @section('content')
<div class="container">
   <div class="row">
        {{-- hero section --}}
      <div class="col-lg-12">
         <div class="jumbotron jumbotron-fluid">
            <div class="container">
               <div class="row">
                  <article class="col-lg-6"></article>
                  <article class="col-lg-6">
                     <h4 class="titles">Manage FAQ</h4>
                     <p class="lead">{{$faq->question}}</p>
                  </article>
               </div>
            </div>
         </div>
      </div>
      {{-- @end of hero section --}}
      {{--  Session flash  --}}
      <div class="col-md-12">
         @if (session('status'))
         <div class="alert alert-success" role="alert">
            {{ session('status') }}
         </div>
         @endif
      </div>
      <div class="col-md-12">
         <h4 class="titles">Manage burning questions</h4>
         <div class="divider"></div>
      </div>
      {{--  New faq  --}}
      <div class="col-md-4">
         <form action="{{ url('update-faq') }}" method="post">
         {{ csrf_field() }}
         <input type="hidden" name="id" value="{{$faq->id}}">
            <div class="form-group">
               <label class="label">Question</label>
               <input value="{{$faq->question}}" class="form-control" placeholder="ask soemthing ..." name="question" required>
            </div>
            <div class="form-group">
               <label class="label">Answer</label>
               <textarea placeholder="Lorem ipsum" rows="10" class="form-control" name="answer" required>{{$faq->answer}}</textarea>
            </div>
            <div class="form-group">
               <button class="btn btn-primary">UPDATE FAQ</button>
            </div>
         </form>
      </div>
      {{--  Burnning question loop  --}}
      <div class="col-md-8">
         @foreach($faqs as $faq)
         <ul class="faq-list">
            <li>
               <h5 class="question">{{$faq->question}}</h5>
               <p class="answer">{{$faq->answer}}
               <br>
               <a class="danger" href="{{ url('archive-faq') }}/{{$faq->id}}">Delete</a> -
               <a href="{{ url('edit') }}/{{$faq->id}}/faq">Edit</a>
            </li>
         </ul>
         @endforeach
         <br>
         <div class="form-group col-lg-10">
            {{ $faqs->links() }}
         </div>
      </div>
      {{--  About website  --}}
      {{--  <div class="col-md-12">
         <br>
         <h4 class="titles">Website</h4>
         <div class="divider"></div>
      </div>
      <div class="col-md-8 offset-lg-2">
      </div>  --}}
   </div>
</div>
@endsection
