<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SCANNED REPORT: VERIPOA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Styles -->
    <style>
        .header-left {
            border: thin solid #e3e3e3;
            width: 45%;
        }
        table,
        html,
        body
        {
            font-size: 13px;
        }
        table > tr > td {
            padding: 3%;
        }
        .title {
            font-weight: bold;
        }
        .titledate {
            text-align: center;
            margin-top: 5%;
        }
        .titledate > h4,
        .titledate h3 {
            font-weight: bold;
            text-decoration: underline;
        }
        p.copyright {
            position: absolute;
            bottom: 2%;
            text-align: center;
            margin-top: 20%;
        }
        .table-details {
            width: 100%;
            /* border: thin solid #e3e3e3; */
        }
        .table-details tr td {
            border-bottom: thin solid #e3e3e3;
            padding: 1%;
        }
        .table > tr {
            border: thin solid #e3e3e3;
        }
        td.tdheader {
            color: #337AB7;
            font-weight: bold;
        }
        #watermark {
            position: absolute;
            z-index: -100;
            left: 50%;
            top: 5%;
            color: #e3e3e3;
        }
        .time {
            color: #4675e3
        }
        .table-data {
            border: thin solid #e9e6ef;
        }
        .table-data tr td {
            border: thin solid #e9e6ef;
            padding: 7px;
        }
        .thead tr td {
            text-align: left;
        }
    </style>
</head>

<body>
<div id="watermark">
    <h1>VERIPOA <br>LTD</h1>
</div>
<div class="container">
    <div class="row">
        <!-- Header -->
        <article class="col-lg-12">
            <h4 class="title">COMPANY DETAILS</h4>
            <div class="header-left">
                <table class="table table-striped table-condensed">
                    <tr>
                        <td>Company Inc.</td>
                        <td>Veripoa</td>
                    </tr>
                    <tr>
                        <td>Email Address.</td>
                        <td>info@veripoa.com</td>
                    </tr>
                    <tr>
                        <td>Phone Number.</td>
                        <td>( 254 ) 733 367 932</td>
                    </tr>
                </table>
            </div>
            <br/>
            <h4 class="title">PRODUCT DETAILS</h4>
            <div class="header-left">
                <?php $product = DB::table('products')->where('id', $id)->first(); ?>
                <table class="table table-striped table-condensed">
                    <tr>
                        <td>Unique ID</td>
                        <td>{{$id}}</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>{{$product->name}}</td>
                    </tr>
                    <tr>
                        <td>Start Date</td>
                        <td>{{$date}}</td>
                    </tr>
                    <tr>
                        <td>End Date</td>
                        <td>{{$date2}}</td>
                    </tr>
                </table>
            </div>
            <div class="titledate">
                <h1>Product</h1>
                <h4>Scanning</h4>
            </div>
        </article>
        <article class="data_container">
            <table class="table-data" style="width:100%">
                <thead class="thead">
                <tr>
                    <th style="text-align: left;">ID</th>
                    <th style="text-align: left;">User</th>
                    <th style="text-align: left;">Status</th>
                    <th style="text-align: left;">Code</th>
                    <th style="text-align: left;">Creation Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $scanned)
                    <tr>
                        <td>{{$scanned-> id}}</td>
                        <td>
                            <?php
                            $names = DB::table('users')->select('name')->where('id', $scanned->user_id)->get();
                            foreach($names as $name) {
                                echo $name->name;
                            }
                            ?>
                        </td>
                        <td>
                            @if($scanned->status == 0) Failed @else Success @endif
                        </td>
                        <td>
                            <?php
                            $results = DB::table('stickers')->select('barcode')->where('id', $scanned->sticker_id)->get();
                            foreach($results as $result) {
                                echo $result->barcode;
                            }
                            ?>
                        </td>
                        <td>{{$scanned-> created_at}}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th style="text-align: left;">ID</th>
                    <th style="text-align: left;">User</th>
                    <th style="text-align: left;">Status</th>
                    <th style="text-align: left;">Code</th>
                    <th style="text-align: left;">Creation Date</th>
                </tr>
                </tfoot>
            </table>
        </article>
        <div class="footer">
            <p align="center" style="font-size: 10px;color: #000;">
                Veripoa <?php echo date('Y'); ?>
            </p>
        </div>
    </div>
</div>
<!-- Body -->
</body>

</html>
