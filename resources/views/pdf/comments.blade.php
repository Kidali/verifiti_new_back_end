<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>COMMENTS STATEMENT REPORT: VERIPOA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Styles -->
    <style>
        .header-left {
            border: thin solid #e3e3e3;
            width: 45%;
        }
        table,
        html,
        body
        {
            font-size: 13px;
        }
        table > tr > td {
            padding: 3%;
        }
        .title {
            font-weight: bold;
        }
        .titledate {
            text-align: center;
            margin-top: 5%;
        }
        .titledate > h4,
        .titledate h3 {
            font-weight: bold;
            text-decoration: underline;
        }
        p.copyright {
            position: absolute;
            bottom: 2%;
            text-align: center;
            margin-top: 20%;
        }
        .table-details {
            width: 100%;
            /* border: thin solid #e3e3e3; */
        }
        .table-details tr td {
            border-bottom: thin solid #e3e3e3;
            padding: 1%;
        }
        .table > tr {
            border: thin solid #e3e3e3;
        }
        td.tdheader {
            color: #337AB7;
            font-weight: bold;
        }
        #watermark {
            position: absolute;
            z-index: -100;
            left: 50%;
            top: 5%;
            color: #e3e3e3;
        }
        .comment_area {
            width: 70%;
            margin: 2% auto;
        }
        .comment_body {
            width: inherit;
            border-bottom: thin solid #e3e3e3;
        }
        .time {
            color: #4675e3
        }
    </style>
</head>

<body>
<div id="watermark">
    <h1>VERIPOA <br>LTD</h1>
</div>
<div class="container">
    <div class="row">
        <!-- Header -->
        <article class="col-lg-12">
            <h4 class="title">COMPANY DETAILS</h4>
            <div class="header-left">
                <table class="table table-striped table-condensed">
                    <tr>
                        <td>Company Inc.</td>
                        <td>Veripoa</td>
                    </tr>
                    <tr>
                        <td>Email Address.</td>
                        <td>info@veripoa.com</td>
                    </tr>
                    <tr>
                        <td>Phone Number.</td>
                        <td>( 254 ) 733 367 932</td>
                    </tr>
                </table>
            </div>
            <br/>
            <h4 class="title">PRODUCT DETAILS</h4>
            <div class="header-left">
                <table class="table table-striped table-condensed">
                    <tr>
                        <td>Unique ID</td>
                        <td>{{$product->id}}</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>{{$product->name}}</td>
                    </tr>
                    <tr>
                        <td>Manufactured Date</td>
                        <td>{{$product->manufactured_date}}</td>
                    </tr>
                </table>
            </div>
            <div class="titledate">
                <h1>Comments</h1>
                <h4>{{ date('m-d-Y') }}</h4>
            </div>
        </article>
        <article class="comment_area">
            @foreach($comments as $comment)
            <div class="comment_body">
                <p class="comment_text">{{$comment->comment}}</p>
                Posted date: <time class="time">{{$comment->created_at}}</time> by:
                <?php $user = DB::table('users')->where('id', $comment->user_id)->first(); ?>
                {{$user->name}}
            </div>
            @endforeach
        </article>
        <div class="footer">
            <p align="center" style="font-size: 10px;color: #000;">
                Veripoa <?php echo date('Y'); ?>
            </p>
        </div>
    </div>
</div>
<!-- Body -->
</body>

</html>
