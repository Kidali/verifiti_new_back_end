@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
         {{-- hero section --}}
      <div class="col-lg-12">
         <div class="jumbotron jumbotron-fluid">
            <div class="container">
               <div class="row">
                  <article class="col-lg-6"></article>
                  <article class="col-lg-6">
                     <h4 class="titles">Manage Settings</h4>
                     <p class="lead">Settings</p>
                  </article>
               </div>
            </div>
         </div>
      </div>
      {{-- @end of hero section --}}
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
        </div>

        <div class="col-md-4 offset-lg-4">

        <form action="{{ url('updatecredentials') }}" method="post">
        {{ csrf_field() }}

        <div class="col-md-12 form-group">
            <h4 align="center" class="titles">Update email</h4>
            <div style="margin: 0 auto;" class="divider"></div>
        </div>
        <div class="form-group">
            <label class="label">Email Address</label>
            <input class="form-control" placeholder="johndoe@somewhere.com" name="email" value="{{Auth::user()->email}}">
        </div>

        <div class="col-md-12 form-group">
            <h4 align="center" class="titles">Update password</h4>
            <div style="margin: 0 auto;" class="divider"></div>
        </div>

        <div class="form-group">
            <label class="label">New password</label>
            <input class="form-control" placeholder="johndoe@somewhere.com" name="password" required>
        </div>

        <div class="form-group">
            <label class="label">Old password</label>
            <input class="form-control" placeholder="johndoe@somewhere.com" name="oldpassword" required>
        </div>

        <div class="form-group">
           <button class="btn btn-primary" type="submit">UPDATE</button>
        </div>



        </form>

        </div>

        <div class="col-md-8 offset-lg-2">
        </div>{{--  End of col-md-8  --}}
    </div>
</div>
@endsection
