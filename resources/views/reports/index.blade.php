@extends('layouts.report') @section('content')
<div class="container">
    <div class="row">
        {{-- hero section --}}
        <div class="col-lg-12">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <div class="row">
                        <article class="col-lg-6"></article>
                        <article class="col-lg-6">
                            <h4 class="titles">Graphical report</h4>
                            <p class="lead">General insight</p>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        {{-- @end of hero section --}}
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
        </div>

        <div class="col-md-8 offset-lg-2">
            <h4 class="titles">General scan visualization</h4>
            <div class="divider"></div>
        </div>

        <div class="col-md-8 offset-2">
      <form action="{{ url('filter-report') }}" method="post">
        {{ csrf_field() }}
           <div class="row">
               <div class="form-group col-lg-4">
                   <label class="labels">Start Date</label>
                   <input type="date" required value="10/24/2018" name="selected_date" class="form-control datepicker">
               </div>
               <div class="form-group col-lg-4">
                   <label class="labels">End Date</label>
                   <input type="date" required value="10/24/2018" name="selected_date2" class="form-control datepicker">
               </div>
               <div class="form-group col-lg-4">
                   <label class="labels">Select Product</label>
                   <select required name="product_id" class="form-control datepicker">
                       @foreach($products as $product)
                           <option value="{{$product->id}}">{{$product->name}}</option>
                       @endforeach
                   </select>
               </div>
               <div class="form-group col-lg-4">
                   <label class="labels">Size</label>
                   <input type="text" placeholder="size" name="size" class="form-control">
               </div>
               <div class="form-group col-lg-4">
                   <label class="labels">Gender</label>
                   <select placeholder="size" name="gender" class="form-control">
                       <option value="M">M</option>
                       <option value="F">F</option>
                   </select>
               </div>
               <div class="form-group col-lg-4">
                   <?php $locations = DB::table('locations')->get(); ?>
                   <label class="labels">County</label>
                   <select name="county" class="form-control">
                       @foreach($locations as $location)
                           <option value="{{$location->name}}">{{$location->name}}</option>
                       @endforeach
                   </select>
               </div>
               <div class="form-group col-lg-4">
                   <label class="labels">Filter:</label><br>
                   <button type="submit" class="btn btn-primary">FILTER RECORD</button>
               </div>
           </div>
        </form>
        </div>

        <div class="col-md-8 offset-lg-2">
            {{--{!! $chart->container() !!}--}}
        </div>

        <div class="col-md-8 offset-lg-2">
            <br>
            <h4 class="titles">Invalid Qrcodes</h4>
            <div class="divider"></div>
        </div>

        <div class="col-md-8 offset-lg-2">
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Code</th>
                        <th>Scanning Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($scans as $scanned)
                    <tr>
                        <td>{{$scanned->id}}</td>
                        <td>
                            <?php
                               $names = DB::table('users')->select('name')->where('id', $scanned->user_id)->get();
                               foreach($names as $name) {
                                   echo $name->name;
                               }
                            ?>
                        </td>
                        <td>{{$scanned->invalid_code}}</td>
                        <td>{{$scanned-> created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Code</th>
                        <th>Scanning Date</th>
                    </tr>
                </tfoot>
            </table>
        </div>{{-- End of col-md-8 --}}
    </div>
</div>
@endsection
