@extends('layouts.report')
@section('content')
<div class="container">
    <div class="row">
         {{-- hero section --}}
      <div class="col-lg-12">
         <div class="jumbotron jumbotron-fluid">
            <div class="container">
               <div class="row">
                  <article class="col-lg-6"></article>
                  <article class="col-lg-6">
                     <h4 class="titles">Graphical report : {{$product->name}}</h4>
                      <p class="lead">Start Date: <span class="activated">{{$date}}</span> - End Date: <span class="activated">{{$date2}}</span></p>
                      <p class="lead">Gender: <span class="activated">{{$gender}}</span></p>
                      <p class="lead">County: <span class="activated">{{$county}}</span></p>
                  </article>
               </div>
            </div>
         </div>
      </div>
      {{-- @end of hero section --}}
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
        </div>

        <div class="col-md-8 offset-lg-2">
            <h4 class="titles">General scan visualization</h4>
            <div class="divider"></div>
        </div>

        <div class="col-md-8 offset-2">
            <form action="{{ url('filter-report') }}" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="form-group col-lg-4">
                        <label class="labels">Start Date</label>
                        <input type="date" required value="10/24/2018" name="selected_date" class="form-control datepicker">
                    </div>
                    <div class="form-group col-lg-4">
                        <label class="labels">End Date</label>
                        <input type="date" required value="10/24/2018" name="selected_date2" class="form-control datepicker">
                    </div>
                    <div class="form-group col-lg-4">
                        <label class="labels">Select Product</label>
                        <select required name="product_id" class="form-control datepicker">
                            @foreach($products as $product)
                                <option value="{{$product->id}}">{{$product->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-lg-4">
                        <label class="labels">Size</label>
                        <input type="text" placeholder="size" name="size" class="form-control">
                    </div>
                    <div class="form-group col-lg-4">
                        <label class="labels">Gender</label>
                        <select placeholder="size" name="gender" class="form-control">
                            <option value="M">M</option>
                            <option value="F">F</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-4">
                        <?php $locations = DB::table('locations')->get(); ?>
                        <label class="labels">County</label>
                        <select name="county" class="form-control">
                            @foreach($locations as $location)
                                <option value="{{$location->name}}">{{$location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-lg-4">
                        <label class="labels">Filter:</label><br>
                        <button type="submit" class="btn btn-primary">FILTER RECORD</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-8 offset-lg-2">
            {!! $chart->container() !!}
        </div>

        <div class="col-md-8 offset-lg-2">
            <br>
            <a href="{{url('download-scanned-report')}}/{{$date}}/{{$date2}}/{{$id}}/{{$size}}/{{$gender}}/{{$county}}/scanned" target="_blank" class="btn btn-primary">DOWNLOAD STATS</a>
        </div>

        {{--  <div class="col-md-8 offset-lg-2">
            <br>
            <h4 class="titles">Users</h4>
            <div class="divider"></div>
        </div>  --}}

        <div class="col-md-8 offset-lg-2">
        </div>{{--  End of col-md-8  --}}
    </div>
</div>
@endsection
