@extends('layouts.report') @section('content')
<div class="container">
    <div class="row">
        {{-- hero section --}}
        <div class="col-lg-12">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <div class="row">
                        <article class="col-lg-6"></article>
                        <article class="col-lg-6">
                            <h4 class="titles">Manage product</h4>
                            <p class="lead">{{$product->name}}</p>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        {{-- @end of hero section --}}
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">

                            <form action="{{ url('update-product') }}" method="post">

                                {{ csrf_field() }}

                                <div class="form-group">
                                    <input type="text" readonly value="{{$product->id}}" name="id" name="id" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Product Name</label>
                                    <input type="text" placeholder="product name" required value="{{$product->name}}" name="productName" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Product Code</label>
                                    <input type="text" placeholder="product code" required value="{{$product->code}}" name="productCode" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Manufacturer</label>
                                    <select placeholder="product name" required name="manufacturer" class="form-control">
                                        <option selected value="{{$product->user[0]->id}}">{{$product->user[0]->name}}</option>
                                        @foreach($partners as $partner)
                                        <option value="{{$partner->id}}">{{$partner->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="labels">RRP</label>
                                    <input type="text" placeholder="0.00" required name="price" value="{{$product->price}}" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Size</label>
                                    <input type="text" placeholder="1 Kg" required value="{{$product->size}}" name="size" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Origin</label>
                                    <input type="text" placeholder="China etc" required value="{{$product->origin}}" name="origin" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Manufacturer Date</label>
                                    <input type="text" name="manufactured_date" class="form-control" value="{{date('d/m/Y', strtotime($product->manufactured_date))}}" />
                                </div>

                                <div class="form-group">
                                    <label class="labels">Expiration Date</label>
                                    <input type="text" required value="{{date('d/m/Y', strtotime($product->expiration_date))}}" name="expiration_date" class="form-control datepicker">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">
                                        UPDATE PRODUCT
                                    </button>
                                    <a href="{{ url('archive-product') }}/{{$product->id}}" class="btn btn-danger">
                                        ARCHIVE
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group col-lg-8 offset-lg-2">
                <br>
                <h4 class="sub-title">Export Data</h4>
                <div class="divider"></div>
            </div>

            <form action="{{ url('exportProductData') }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="product_id" value="{{$product->id}}">

                <div class="row">
                <div class="form-group col-lg-12">
                    <label class="labels">Select Data</label>
                    <select placeholder="product name" required name="datatype" id="datatype" class="form-control">
                        <option selected value="1">Stickers</option>
                        <option value="2">Scanned History</option>
                        <option value="3">Comments</option>
                    </select>
                </div>

                <div class="form-group col-lg-6">
                    <label class="labels">Start Date</label>
                    <input type="date" required value="10/24/2018" name="selected_date" class="form-control datepicker">
                </div>

                <div class="form-group col-lg-6">
                    <label class="labels">Start time</label>
                    <input type="time" name="startTime" class="form-control timepicker" value="00:00:00" />
                </div>

                <div class="form-group col-lg-6">
                    <label class="labels">End Date</label>
                    <input type="date" required value="10/24/2018" name="selected_date2" class="form-control datepicker">
                </div>

                <div class="form-group col-lg-6">
                    <label class="labels">End time</label>
                    <input type="time" name="endTime" class="form-control endTime" value="00:00:00" />
                </div>

                <div class="form-group col-lg-12">
                    <label class="labels">Format</label>
                    <input readonly id="export_format" placeholder="Format type" required name="exportformat" class="form-control">
                </div>

                <div class="form-group col-lg-8 offset-lg-2">
                    <button type="submit" class="btn btn-primary">Generate</button>
                </div>
                </div>
            </form>

            <br>
            <br>

            <div class="form-group col-lg-8 offset-lg-2">
                <br>
                <h4 class="sub-title">Social Sharing</h4>
                <div class="divider"></div>
            </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="circle-stats">
                        <h4 class="circle-stats-number">{{$socialsharings->facebook}}</h4>
                    </div>
                    <br>
                    <h4 align="center" class="social-m">Facebook</h4>
                </div>

                <div class="col-lg-12">
                    <div class="circle-stats">
                        <h4 class="circle-stats-number">{{$socialsharings->whatsapp}}</h4>
                    </div>
                    <br>
                    <h4 align="center" class="social-m">WhatsApp</h4>
                </div>

                <div class="col-lg-12">
                    <div class="circle-stats">
                        <h4 class="circle-stats-number">{{$socialsharings->email}}</h4>
                    </div>
                    <br>
                    <h4 align="center" class="social-m">Email</h4>
                </div>

                <div class="col-lg-12">
                    <div class="circle-stats">
                        <h4 class="circle-stats-number">{{$socialsharings->twitter}}</h4>
                    </div>
                    <br>
                    <h4 align="center" class="social-m">Twitter</h4>
                </div>

            </div>

        </div>
        <div class="col-md-8">
            <div class="form-group">
                <h4 class="sub-title">Stickers</h4>
                <div class="divider"></div>
            </div>
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Code</th>
                        <th>Serial Number</th>
                        <th>Status</th>
                        <th>Creation Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($stickers as $sticker)
                    <tr>
                        <td>{{$sticker-> id}}</td>
                        <td>{{$sticker->barcode}}</td>
                        <td>{{$sticker->serial_number}}</td>
                        <td>@if($sticker->status == 0) Used @else New @endif</td>
                        <td>{{$sticker-> created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Code</th>
                        <th>Serial Number</th>
                        <th>Status</th>
                        <th>Creation Date</th>
                    </tr>
                </tfoot>
            </table>
            <div class="form-group">
                <h4 class="sub-title">Scanned history</h4>
                <div class="divider"></div>
            </div>
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Status</th>
                        <th>Code</th>
                        <th>Creation Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($product->scanned as $scanned)
                    <tr>
                        <td>{{$scanned-> id}}</td>
                        <td>
                            <?php
                               $names = DB::table('users')->select('name')->where('id', $scanned->user_id)->get();
                               foreach($names as $name) {
                                   echo $name->name;
                               }
                            ?>
                        </td>
                        <td>
                            @if($scanned->status == 0) Failed @else Success @endif
                        </td>
                        <td>
                            <?php
                               $results = DB::table('stickers')->select('barcode')->where('id', $scanned->sticker_id)->get();
                               foreach($results as $result) {
                                   echo $result->barcode;
                               }
                            ?>
                        </td>
                        <td>{{$scanned-> created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Status</th>
                        <th>Code</th>
                        <th>Creation Date</th>
                    </tr>
                </tfoot>
            </table>
            <div class="form-group">
                <h4 class="sub-title">Comments sampling</h4>
                <div class="divider"></div>
            </div>
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($comments as $comment)
                    <tr>
                        <td>{{$comment-> id}}</td>
                        <td>
                            {{$comment->comment}}
                            <br>
                            <time class="activated">{{$comment->created_at}}</time>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Comment</th>
                    </tr>
                </tfoot>
            </table>
            <div class="form-group">
                <h4 class="sub-title">Graphical presentation</h4>
                <div class="divider"></div>
            </div>
            <div class="form-group">
                {!! $chart->container() !!}
            </div>
        </div>
    </div>
</div>
@endsection
