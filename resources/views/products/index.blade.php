@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
         {{-- hero section --}}
      <div class="col-lg-12">
         <div class="jumbotron jumbotron-fluid">
            <div class="container">
               <div class="row">
                  <article class="col-lg-6"></article>
                  <article class="col-lg-6">
                     <h4 class="titles">Manage products</h4>
                     <p class="lead">Products</p>
                  </article>
               </div>
            </div>
         </div>
      </div>
      {{-- @end of hero section --}}
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">

                            <form action="{{ url('add-product') }}" method="post">

                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label class="labels">Product Name</label>
                                    <input type="text" placeholder="product name" required name="productName" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Product Code</label>
                                    <input type="text" placeholder="product code" required name="productCode" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Manufacturer</label>
                                    <select placeholder="product name" required name="manufacturer" class="form-control">
                                    @foreach($partners as $partner)
                                        <option value="{{$partner->id}}">{{$partner->name}}</option>
                                    @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="labels">RRP</label>
                                    <input type="text" placeholder="0.00" required name="price" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Size</label>
                                    <input type="text" placeholder="1 Kg" required name="size" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Origin</label>
                                    <input type="text" placeholder="China etc" required name="origin" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="labels">Manufacture Date</label>
                                    <input type="text" name="manufactured_date" class="form-control" value="10/24/2018" />
                                </div>

                                <div class="form-group">
                                    <label class="labels">Expiration Date</label>
                                    <input type="text" value="10/24/2018" required name="expiration_date" class="form-control datepicker">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">
                                        SAVE PRODUCT
                                    </button>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Product</th>
                        <th>Code</th>
                        <th>Size</th>
                        <th>Manufacturer</th>
                        <th>Creation Date</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td><a href="{{ url('manage-product') }}/{{$product->id}}">{{$product->name}}</a></td>
                        <td>{{$product->code}}</td>
                        <td>{{$product->size}}</td>
                        <td>
                            <?php
                               $results = DB::table('users')->select('name')->where('id', $product->user_id)->get();
                               foreach($results as $result) {
                                   echo $result->name;
                               }
                            ?>
                        </td>
                        <td>{{$product->created_at}}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Product</th>
                        <th>Code</th>
                        <th>Serial Number</th>
                        <th>Status</th>
                        <th>Creation Date</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
