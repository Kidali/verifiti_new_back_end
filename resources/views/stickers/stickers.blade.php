@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
        </div>

        <div class="col-md-8 offset-lg-2">
            <h4 class="titles">Manage stickers</h4>
            <div class="divider"></div>
        </div>

        <div class="col-md-8 offset-lg-2">
            <h5 class="titles">Generate stickers</h5>
        </div>

        <div class="col-md-8 offset-lg-2">
            <div class="row">
                <div class="col-lg-4 right-side">
                    <form action="{{ url('generatesticker') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="label">Requested Number</label>
                            <input type="text" class="form-control" name="number" required placeholder="23">
                        </div>
                        <div class="form-group">
                            <label class="label">Client</label>
                            <select type="text" class="form-control" name="user_id" required placeholder="Calos LTD">
                                @foreach($clients as $client)
                                    <option value="{{$client->id}}">{{$client->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="label">Product</label>
                            <select type="text" class="form-control" name="product_id" required placeholder="OMO">
                            @foreach($products as $prod)
                            <option value="{{$prod->id}}">{{$prod->name}}</option>
                            @endforeach
                            </select>
                        </div>
                            <input type="hidden" class="form-control" name="serial_number" placeholder="AUTO GENERATE">
                        <div class="form-group">
                            <button class="btn btn-primary">GENERATE</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-8">

                    <div class="form-group floats col-lg-4">
                        <div class="division col-lg-12">
                            <h5 class="stats-text">{{ $stickersnotused }}<br/>Not used</h5>
                        </div>
                    </div>

                    <div class="form-group floats col-lg-4">
                        <div class="division col-lg-12">
                            <h5 class="stats-text">{{ $stickersused }}<br/>Used</h5>
                        </div>
                    </div>

                    <div class="form-group floats col-lg-4">
                        <div class="division col-lg-12">
                            <h5 class="stats-text">{{ $stickersused + $stickersnotused }}<br/>Total</h5>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-8 offset-lg-2">
            <table class="table table-striped table-bordered table_data" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Product</th>
                        <th>Code</th>
                        <th>Serial Number</th>
                        <th>Status</th>
                        <th>Creation Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($stickers as $sticker)
                    <tr>
                        <td>{{ $sticker->id }}</td>
                        <td>
                            <?php
                            $products = DB::table('products')->where('id', $sticker->product_id)->get();
                         ?>
                                @foreach($products as $product) {{$product->name}} @endforeach
                        </td>
                        <td>{{ $sticker->barcode }}</td>
                        <td>{{ $sticker->serial_number }}</td>
                        <td>
                            @if($sticker->status == 0) Repeat @elseif($sticker->status == 1) Genuine @else Invalid @endif
                        </td>
                        <td>{{ $sticker->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Product</th>
                        <th>Code</th>
                        <th>Serial Number</th>
                        <th>Status</th>
                        <th>Creation Date</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
