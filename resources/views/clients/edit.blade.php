@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row">
      {{-- hero section --}}
      <div class="col-lg-12">
         <div class="jumbotron jumbotron-fluid">
            <div class="container">
               <div class="row">
                  <article class="col-lg-6"></article>
                  <article class="col-lg-6">
                     <h4 class="titles">Manage partner</h4>
                     <p class="lead">{{$user->name}}</p>
                  </article>
               </div>
            </div>
         </div>
      </div>
      {{-- @end of hero section --}}
      <div class="col-md-4">
         <div class="card">
            <div class="card-body">
               @if (session('status'))
               <div class="alert alert-success" role="alert">
                  {{ session('status') }}
               </div>
               @endif
               <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                     <form action="{{ url('update-client') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                           <input class="form-control" readonly name="id" value="{{$user->id}}">
                        </div>
                        <div class="form-group">
                           <label class="labels">Full Names</label>
                           <input type="text" value="{{ $user->name }}" name="name" class="border-none form-control">
                        </div>
                        <div class="form-group">
                           <label class="labels">Email Address</label>
                           <input type="text" value="{{ $user->email }}" name="email" class="border-none form-control">
                        </div>
                        <div class="form-group">
                           <label class="labels">Phone Number</label>
                           <input type="text" value="{{ $user->phone_number }}" name="phone_number" class="border-none form-control">
                        </div>
                        <div class="form-group">
                           {{--  <label class="labels">Gender</label>  --}}
                           <select style="display: none;" type="text" name="gender" class="border-none form-control">
                             @if($user->gender == 'F')
                              <option value="M">Male</option>
                              <option selected value="F">Female</option>
                             @else
                              <option value="F">Female</option>
                              <option selected value="M">Male</option
                            @endif
                           </select>
                        </div>
                        <div class="form-group">
                           <label class="labels">Status</label>
                           <input type="text" value="@if($user->status == 1)Not Active
                              @else Active
                              @endif" readonly class="border-none form-control">
                        </div>
                        <div class="form-group">
                           <button type="submit" class="btn btn-primary">UPDATE USER</button>
                           <a href="{{ url('archive-partner') }}/{{ $user->id }}" class="btn btn-danger">
                           DEACTIVATE
                           </a>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      {{--  Display scanned history  --}}
      <div class="col-md-8">
         <div class="form-group">
            <h4 class="sub-title">Products</h4>
            <div class="divider"></div>
         </div>
         <table class="table table-striped table-bordered table_data" style="width:100%">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Product</th>
                  <th>Code</th>
                  <th>Size</th>
                  <th>Manufacturer</th>
                  <th>Creation Date</th>
               </tr>
            </thead>
            <tbody>
               @foreach($products as $product)
               <tr>
                  <td>{{$product->id}}</td>
                  <td><a href="{{ url('manage-product') }}/{{$product->id}}">{{$product->name}}</a></td>
                  <td>{{$product->code}}</td>
                  <td>{{$product->size}}</td>
                  <td>
                     <?php
                        $results = DB::table('products')->select('name')->where('id', $product->user_id)->get();
                        foreach($results as $result) {
                            echo $result->name;
                        }
                        ?>
                  </td>
                  <td>{{$product->created_at}}</td>
               </tr>
               @endforeach
            </tbody>
            <tfoot>
               <tr>
                  <th>ID</th>
                  <th>Product</th>
                  <th>Barcode</th>
                  <th>Serial Number</th>
                  <th>Status</th>
                  <th>Creation Date</th>
               </tr>
            </tfoot>
         </table>
      </div>
   </div>
</div>
@endsection
