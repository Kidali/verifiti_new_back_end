@extends('layouts.app') @section('content')
<div class="container">
   <div class="row">
      {{--  hero section  --}}
      <div class="col-lg-12">
         <div class="jumbotron jumbotron-fluid">
            <div class="container">
               <div class="row">
                  <article class="col-lg-6"></article>
                  <article class="col-lg-6">
                     <h4 class="titles">Manage Partners</h4>
                     <p class="lead">Partners</p>
                  </article>
               </div>
            </div>
         </div>
      </div>
      {{--  @end of hero section  --}}
      <div class="col-md-12">
         <div class="col-md-3 offset-lg-10">
            <button data-toggle="modal" data-target="#exampleModal" class="btn btn-primary">ADD CLIENT</button>
         </div>
         {{--  content  --}}
         <table class="table table-striped table-bordered table_data" style="width:100%">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  {{--  <th>Gender</th>  --}}
                  <th>Status</th>
                  <th>Creation Date</th>
                  <th></th>
               </tr>
            </thead>
            <tbody>
               @foreach($users as $user)
               <tr>
                  <td>{{ $user -> id }}</td>
                  <td>{{ $user -> name }}</td>
                  <td>{{ $user -> email }}</td>
                  <td>{{ $user -> phone_number }}</td>
                  {{--  <td>{{ $user -> gender }}</td>  --}}
                  <td>
                     @if($user->status == 1)
                     Not Active
                     @else
                     Active
                     @endif
                  </td>
                  <td>{{ $user -> created_at }}</td>
                  <td><a href="{{ url('edit') }}/{{ $user->id}}/partner">Manage user</a></td>
               </tr>
               @endforeach
            </tbody>
            <tfoot>
               <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  {{--  <th>Gender</th>  --}}
                  <th>Staatus</th>
                  <th>Creation Date</th>
                  <th></th>
               </tr>
            </tfoot>
         </table>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">New client</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-lg-12">
                  <form action="{{ url('add-client') }}" method="post">
                     {{ csrf_field() }}
                     <div class="form-group">
                        <label class="labels">Full Names</label>
                        <input type="text" name="name" placeholder="John Doe" class="border-none form-control">
                     </div>
                     <div class="form-group">
                        <label class="labels">Email Address</label>
                        <input type="text" name="email" placeholder="Johndoe@somewhere.com" class="border-none form-control">
                     </div>
                     <div class="form-group">
                        <label class="labels">Phone Number</label>
                        <input type="text" name="phone_number" placeholder="07xx xxx xxx" class="border-none form-control">
                     </div>
                     <div class="form-group">
                        {{--  <label class="labels">Gender</label>  --}}
                        <select style="display: none" type="text" name="gender" class="border-none form-control">
                           <option value="M">Male</option>
                           <option value="F">Female</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <button type="submit" class="btn btn-primary">SAVE CLIENT</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         {{--
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
         </div>
         --}}
      </div>
   </div>
</div>
@endsection
