@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

             <div class="col-lg-4 floats">
                 <div class="col-lg-12 form-group dashboard-cover-stats">
                    <div class="circle-stats">{{$productCount}}</div>
                    <h4 class="dashboard-stats">Products</h4>
                 </div>
             </div>

             <div class="col-lg-4 floats">
                 <div class="col-lg-12 form-group dashboard-cover-stats">
                    <div class="circle-stats">{{$stickersCount}}</div>
                    <h4 class="dashboard-stats">Stickers</h4>
                 </div>
             </div>

            <div class="col-lg-4 floats">
                 <div class="col-lg-12 form-group dashboard-cover-stats">
                    <div class="circle-stats">{{$clientCount}}</div>
                    <h4 class="dashboard-stats">Clients</h4>
                 </div>
             </div>


            <div class="col-lg-4 floats offset-lg-4">
                 <div class="col-lg-12 form-group dashboard-cover-stats">
                    <div class="circle-stats">{{$customerCount}}</div>
                    <h4 class="dashboard-stats">App users</h4>
                 </div>
             </div>

        </div>
    </div>
</div>
@endsection
