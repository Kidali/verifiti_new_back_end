<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/general.css') }}" rel="stylesheet">
      <link href="{{ asset('@chenfengyuan/datepicker/js/dist/datepicker.css') }}">
      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
      <link href="{{ asset('css/datepicker.css') }}">
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
   </head>
   <body>
      <div id="app">
         @if(Auth::user())
         <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
               <a class="navbar-brand" href="{{ url('/') }}">
               Verifiti
               </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <!-- Left Side Of Navbar -->
                  <ul class="navbar-nav mr-auto">
                     <li class="nav-link"><a href="{{ url('home') }}">Dashboard</a></li>
                  </ul>
                  <!-- Right Side Of Navbar -->
                  <ul class="navbar-nav ml-auto">
                     <!-- Authentication Links -->
                     @guest
                     {{--
                     <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                     </li>
                     --}}
                     @if (Route::has('register'))
                     {{--
                     <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                     </li>
                     --}}
                     @endif @else
                     <li class="nav-link"><a href="{{ url('manage-products') }}">Products</a></li>
                     <li class="nav-link"><a href="{{ url('manage-stickers') }}">Stickers</a></li>
                     <li class="nav-link"><a href="{{ url('manage-users') }}">Users</a></li>
                     <li class="nav-link"><a href="{{ url('manage-partners') }}">Partners</a></li>
                          <li class="nav-link"><a href="{{ url('manage-website-content') }}">Website Content</a></li>
                          <li class="nav-link"><a href="{{ url('chat') }}">Chat</a></li>
                     <li class="nav-link"><a href="{{ url('manage-report') }}">Report</a></li>
                     <li class="nav-link"><a href="{{ url('manage-settings') }}">Settings</a></li>
                     <li class="nav-link"><a href="{{ url('manage-archived') }}">Archived</a></li>
                     <li>
                     <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                           <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                           {{ __('Logout') }}
                           </a>
                           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                           </form>
                        </div>
                     </li>
                     @endguest
                  </ul>
               </div>
            </div>
         </nav>
         @endif
         <main class="py-4">
            @yield('content')
            <div class="row">
               <div class="col-md-8 offset-lg-2">
                  <div class="footer">
                     <h4 align="center" class="footer-text">&copy; 2019.Inc ALL RIGHTS RESERVED</h4>
                  </div>
               </div>
            </div>
         </main>
      </div>
      {{-- Scripts --}}
      <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.bootstrap4.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
      <script>
         $(document).ready(function() {
             $('.table_data').DataTable({
                 buttons: [
                     'copy', 'excel', 'pdf'
                 ]
             });
         });
      </script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
   </body>
</html>
