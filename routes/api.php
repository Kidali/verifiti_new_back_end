<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// before
Auth::routes();

// Api Version 1
Route::group([
    'prefix' => 'v1/auth/',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('password/reset', 'AuthController@reset');
    Route::post('create-password-reset', 'AuthController@create');

    Route::get('signup/activate/{token}', 'AuthController@signupActivate');
    Route::get('password/find/{token}', 'AuthController@find');

    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('locations', 'ApiController@locations');

        Route::get('about-company', 'ApiController@aboutcompay');
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
        Route::get('getAllMessages/{id}', 'ApiController@getAllMessages');
        Route::post('updateProfile', 'AuthController@updateProfile');
        Route::post('updatecompanyinfo', 'ApiController@updatecompanyinfo');

        // Stickers
        Route::get('stickers', 'StickersController@stickers');
        Route::get('archivesticker/{id}', 'StickersController@archivesticker');
        Route::get('archivescanned/{id}', 'StickersController@archivescanned');
        Route::get('searchBarcode/{barcode}/{user_id}', 'StickersController@searchBarcode');
        Route::post('updatesticker', 'StickersController@updatesticker');
        Route::post('generatesticker', 'StickersController@generatesticker');
        Route::get('scannedstickers/{id}', 'StickersController@scannedstickers');

        // Product
        Route::post('addproducts', 'ApiController@addproducts');

        // Faqs
        Route::get('faqs', 'ApiController@faqs');
        Route::get('deletefaq/{id}', 'ApiController@delclearetefaqs');
        Route::post('updatefaq', 'ApiController@updatefaq');

        // Send mail
        Route::post('sendsupport', 'ApiController@sendsupport');

        // Stats & Comments
        Route::post('sendcomment', 'ApiController@createcomment');
        Route::post('sendstats', 'ApiController@createstats');

        // Points
        Route::post('redeempoints', 'ApiController@redeempoints');

        // Website content
        Route::get('about', 'ApiController@about');

        // Record Shares
        Route::post('registershares', 'ApiController@registershares');

        // Chat
        Route::resource('chat', 'ChatController');
    });
});
