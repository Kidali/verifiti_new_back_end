<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// GET
Route::get('/manage-stickers', 'StickersController@index')->name('managestickers');
Route::get('/manage-settings', 'SettingsController@index')->name('managesettings');
Route::get('/manage-users', 'ConsumersController@index')->name('manageconsumers');
Route::get('/manage-partners', 'ClientsController@index')->name('managepartners');
Route::get('/manage-website-content', 'WebsiteController@index')->name('managewebsitecontent');
Route::get('/manage-report', 'ReportController@index')->name('managereports');
Route::get('/manage-products', 'ProductsController@index')->name('manageproducts');
Route::get('/manage-product/{id}', 'ProductsController@manageproduct')->name('manageproduct');
Route::get('/manage-archived', 'ArchivedController@index')->name('managearchived');
Route::get('firebase','FirebaseController@index');

Route::resource('chat', 'ChatController');
Route::get('/reply-to-chat/{id}', 'ChatController@edit')->name('replytochat');
Route::get('/chat-thread/{id}', 'ChatController@edit')->name('chatthread');
Route::post('/createthread', 'ChatController@store');
Route::post('/sendmessage', 'ChatController@sendmessage');

// POST
Route::post('/add-client', 'ClientsController@createclient')->name('addclient');
Route::post('/generate-stickers', 'StickersController@createstickers')->name('generatestickers');
Route::post('/add-product', 'ProductsController@createproduct')->name('addproduct');
Route::post('/add-consumer', 'ConsumersController@createconsumer')->name('addconsumer');
Route::post('/add-faq', 'WebsiteController@createfaq')->name('addfaq');

// UPDATE
Route::post('/update-client', 'ClientsController@updateclient')->name('updateclient');
Route::post('/update-sticker', 'StickersController@updatesticker')->name('updatesticker');
Route::post('/update-product', 'ProductsController@updateproduct')->name('updateproduct');
Route::post('/update-consumer', 'ConsumersController@updateconsumer')->name('updateconsumer');
Route::post('/update-faq', 'WebsiteController@updatefaq')->name('updatefaq');
Route::post('/updatecredentials', 'SettingsController@updatecredentials')->name('updatecredentials');

// EDIT
Route::get('/edit/{id}/partner', 'ClientsController@edit')->name('editclient');
Route::get('/edit/{id}/user', 'ConsumersController@edit')->name('editclient');
Route::get('/edit/{id}/sticker', 'StickersController@editfaq')->name('editsticker');
Route::get('/edit/{id}/faq', 'WebsiteController@editfaq')->name('editclient');

// DELETE
Route::get('/archive-consumer/{id}', 'ConsumersController@archive')->name('archiveconsumer');
Route::get('/archive-partner/{id}', 'ClientsController@archive')->name('archiveclients');
Route::get('/archive-sticker', 'StickersController@archive')->name('archivesticker');
Route::get('/archive-product/{id}', 'ProductsController@archive')->name('archiveproduct');
Route::get('/archive-faq/{id}', 'WebsiteController@deletefaq')->name('archivefaq');

// UN ARCHIVE
Route::get('/product-restore/{id}', 'ArchivedController@productrestore')->name('productrestore');
Route::get('/user-restore/{id}', 'ArchivedController@restoreuser')->name('restoreuser');

// STICKER
Route::post('generatesticker', 'StickersController@generatesticker');

// REPORT
Route::post('/exportProductData', 'ReportController@exportProductData');
Route::post('/filter-report', 'ReportController@filterreport');
Route::get('/filter-scanned/{date}/{date2}/{id}/{size}/{gender}/{county}/scanned', 'ReportController@filter');
Route::get('/download-scanned-report/{date}/{date2}/{id}/{size}/{gender}/{county}/scanned', 'ReportController@downloadscannedreport');
